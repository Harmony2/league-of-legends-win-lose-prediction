# League of Legends Win-Lose Prediction

Data analysis of high diamond - low master 10-minutes-mark matches, including blue and red team performances on kills and objectives.
Hypothesis: We can use machine learning to, based on the first 10 minutes of the game, predict an match outcome.